module.exports = (number) => {
    return number % 2 == 0;
}